
/**
 * Bài 1
 * Đầu vào: Số ngày làm 
 * Phương pháp tính: Tổng số ngày làm * 100.000
 * Đầu ra: Tổng số tiền lương 
 */

function tinhLuong(){
    var tienLuong = document.getElementById("tienLuong").value*1;
    var ngayLam = document.getElementById("ngayLam").value*1;
    document.getElementById("result").innerText = `Số tiền làm là: ${tienLuong*ngayLam}`;
}

/**
 * Bài 2
 * Đầu vào: Các số thực 
 * Phương pháp tính: Tổng tất cả các chữ số chia 5
 * Đầu ra: Trung bình của các số thực
 */

function trungbinhTong(){
    var a = document.getElementById("so1").value*1;
    var b = document.getElementById("so2").value*1;
    var c = document.getElementById("so3").value*1;
    var d = document.getElementById("so4").value*1;
    var e = document.getElementById("so5").value*1;
    document.getElementById("result").innerText = `Trung bình tổng của 5 số là: ${(a+b+c+d+e)/5}`;
}

/**
 * Bài 3
 * Đầu vào:Số tiền USD
 * Phương pháp tính:Số tiền USD * 23500
 * Đầu ra: Số tiền sau khi quy đổi
 */

function quyDoi(){
    var soTien = document.getElementById("tienUSD").value*1;
    var result = soTien*23500;
    document.getElementById("result").innerText =`Số tiền quy đổi là: ${new Intl.NumberFormat('vn-VN').format(result)}$`
}

/**
 * Đầu vào: Chiều dài và chiều rộng hình chữ nhật
 * Phương pháp tính: 
 * + Đối với chu vi: (Chiều dài + chiều rộng ) * 2
 * + Đối với diện tích: Chiều dài * chiều rộng
 * Đầu ra: Chu vi và diện tích hình chữ nhật
 */
function Tinh(){
    var height = document.getElementById("height").value*1;
    var width = document.getElementById("width").value*1;
    document.getElementById("chuVi").innerText =`Chu vi hình chữ nhật là: ${(width+height)*2}`;
    document.getElementById("dienTich").innerText =`Diện tích hình chữ nhật là: ${width*height}`;
}

/**
 * Đầu vào: Số có 2 chữ số
 * Phương pháp tính: 
 * + Chia lấy dư chữ số hàng đơn vị gán với 1 giá trị: n % 10
 * + Chia lấy thương chữ số hàng đơn vị gán với 1 giá trị: n / 10
 * + Lấy giá trị ở hàng đơn vị + giá trị ở hàng chục
 * Đầu ra: Tổng của số có 2 chữ số
 */
function tinhTong(){
    var number = document.getElementById("so").value*1;
    var n_don_vi = number % 10;
    var n_hang_chuc = Math.floor(number / 10);
    var sum = n_don_vi + n_hang_chuc;
    document.getElementById("result").innerText=`Tổng của số có hai chữ số là: ${sum}`;
}